var title = document.createElement('h1');
title.innerHTML = "Valid Uri Generator";
document.body.appendChild(title);

/* This section creates the input form which will be
 * used by the user to enter the programme id which
 * will then be used as part of the uri at the end.
 * it uses the same createElement as before to make
 * an input tag. The type of input(number for integer),
 * name of input and the starting value are all set
 * as well. The appendChild at the end places the input
 * within the previously created form.
*/
var pHeader = document.createElement('h3');
pHeader.innerHTML = "Enter Programme ID :";
document.body.appendChild(pHeader);

var pInput = document.createElement('input');
pInput.type = 'number';
pInput.name = 'programmeID';
pInput.value = 'Programme ID';
pInput.setAttribute('id', 'pID');


document.body.appendChild(pHeader);
document.body.appendChild(pInput);

/* The section below is the same as above but is adapted
 * so that it will take the channel id that the user inputs
 * instead of the programme id
*/

var cHeader = document.createElement('h3');
cHeader.innerHTML = "Enter Channel ID :";
document.body.appendChild(cHeader);

var cInput = document.createElement('input');
cInput.type = 'number';
cInput.name = 'channelID';
cInput.value = 'Channel ID';
cInput.setAttribute('id', 'cID');

document.body.appendChild(cHeader);
document.body.appendChild(cInput);

/* This next section creates the input form which will be
 * used by the user to enter the redirect which
 * will then be used as part of the uri at the end.
 * It uses the same createElement as before to make
 * the input tag. The type is set to string, the name
 * to redirect and has a starting value of redirect.
 * The appendChild at the end places the input within
 * the previously created form.
*/

var rHeader = document.createElement('h3');
rHeader.innerHTML = "Enter Redirect To :";
document.body.appendChild(rHeader);

var rInput = document.createElement('input');
rInput.type = 'String';
rInput.name = 'redirect';
rInput.value = 'Redirect to';
rInput.setAttribute('id', 'redirect');

document.body.appendChild(rHeader);
document.body.appendChild(rInput);


/* This next variable will be used to create the
 * button element that will be clicked. Once clicked
 * it will call the uri() function.
 */

var Button = document.createElement('button');
Button.innerHTML ='Generate Uri';
Button.setAttribute('onclick', 'uri()');
document.write("\n");

document.body.appendChild(Button);

/* This function will get the values that the user
 * has entered into each input form and then adds them
 * to the uri line that appears once the function is run
*/

function uri(){
  var programmeID = document.getElementById('pID').value;
  var channelID = document.getElementById('cID').value;
  var redirect = document.getElementById('redirect').value;

  var uri =document.createElement('h3');
  uri.innerHTML = "Your URI is : http://localhost:3000/ww/form_builder/forms/14/completions/new?programme_id="+ programmeID + "&channel_id=" + channelID +"&redirect_to=/" + redirect;
  document.body.appendChild(uri);
}
